import 'dart:convert';

import 'package:cosmetic_app/Repository/product_repository.dart';
import 'package:cosmetic_app/models/product_model.dart';
import 'package:flutter/material.dart';

class HomeProviderController extends ChangeNotifier{
  final _productRepository = ProductRepository();
  List<Product> _products = [];
  List<Product> get products => _products;

  List<bool> _favorites = [];
  List<bool> get favorites => _favorites;

  final List<Product> _items = [];

  void addFavorite(String id) {
    for (var i = 0; i <= _products.length; i++) {
      if (i.toString() == id) {
        _items.add(products[i]);
      } else {
        debugPrint('=================erty');
      }
    }
    notifyListeners();
  }

  void loadProducts() async {
    try {
      var pro = await _productRepository.fetchProducts();
      if(pro != null){
        _products = pro;
        notifyListeners();
      }
      debugPrint("========** ${jsonEncode(products)}");
    } catch (e) {
      debugPrint('Failed to fetch products: $e');
    }
    notifyListeners();
  }
}