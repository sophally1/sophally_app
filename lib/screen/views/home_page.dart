import 'package:carousel_slider/carousel_slider.dart';
import 'package:cosmetic_app/app_controller/home_controller.dart';
import 'package:cosmetic_app/screen/Bottom_screen/profile_screen.dart';
import 'package:cosmetic_app/screen/product_details.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
    Provider.of<HomeProviderController>(context, listen: false).loadProducts();
  }

  final List<Widget> _widgetOptions = <Widget>[
    const Text('Home Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Discover Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Rewards Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Discuss Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Profile Page',
        style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  final List<String> imgList = [
    'https://www.voisins.com/wp-content/uploads/WEBBANNER-1980-X-1080.jpg',
    'https://lh3.googleusercontent.com/ym5zTX0rFKWe_2p5mIoKO_gHgtjouegbOJ5NgcxmWYXqWg97JZB26mscv2AWhypGaDOzpm7IxSbmPecLwXpHExP0QWaDmtOoBA=w1200-h630-rj-pp-e365',
    'https://static.vecteezy.com/system/resources/thumbnails/011/997/519/small_2x/cosmetic-social-media-post-template-bundle-banner-set-free-vector.jpg',
  ];

  final List<String> categories = [
    'Cleansers',
    'Exfoliators',
    'Toners',
    'Treatments',
    'Masks',
    'Moisturizers'
  ];
  final List<IconData> categoryIcons = [
    Icons.clean_hands,
    Icons.spa,
    Icons.local_drink,
    Icons.medical_services,
    Icons.face,
    Icons.wash,
  ];
  @override
  Widget build(BuildContext context) {
    final pro = context.watch<HomeProviderController>();
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(50),
        child: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: CircleAvatar(
              backgroundColor: Colors.grey[300],
              radius: 16.0,
              child: ClipOval(
                child: Image.asset(
                  'assets/images/profile.JPG',
                  fit: BoxFit.cover,
                  width: 32.0, // Double the radius value
                  height: 32.0, // Double the radius value
                ),
              ),
            ),
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const ProfileScreen(),
                  ));
            },
          ),
          title: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8.0),
            child: TextField(
              decoration: InputDecoration(
                filled: true,
                fillColor: Colors.grey[200],
                contentPadding: const EdgeInsets.symmetric(vertical: 0),
                prefixIcon: const Icon(Icons.search, color: Colors.grey),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(30.0),
                  borderSide: BorderSide.none,
                ),
                hintText: 'Search',
              ),
            ),
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.notifications, color: Colors.grey),
              onPressed: () {},
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(
                'What are you looking for?',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              height: 110,
              margin: const EdgeInsets.symmetric(vertical: 10),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: categories.length,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      children: [
                        CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.pinkAccent.withOpacity(0.5),
                          child:
                          Icon(categoryIcons[index], color: Colors.white),
                        ),
                        const SizedBox(height: 5),
                        Text(
                          categories[index],
                          style: const TextStyle(color: Colors.grey),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: CarouselSlider(
                options: CarouselOptions(
                  height: 200.0,
                  viewportFraction: 1,
                  autoPlay: true,
                  enlargeCenterPage: true,
                  aspectRatio: 16 / 9,
                  autoPlayInterval: const Duration(seconds: 3),
                ),
                items: imgList
                    .map((item) => ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Image.network(item,
                      fit: BoxFit.cover, width: 1000),
                ))
                    .toList(),
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10),
              child: Text(
                'New and Trending K-Beauty',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            if (pro.products.isNotEmpty) ...[
              Padding(
                padding: const EdgeInsets.all(10),
                child: GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.all(10),
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10,
                    childAspectRatio: 2 / 3,
                  ),
                  itemCount: pro.products.length,
                  itemBuilder: (context, index) {
                    if (pro.products.isEmpty || index >= pro.products.length) {
                      return const Center(child: Text("No products available"));
                    }
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 8.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16),
                          boxShadow: const [
                            BoxShadow(
                              color: Colors.black26,
                              blurRadius: 4,
                              offset: Offset(2, 2),
                            ),
                          ],
                        ),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => ProductDetails(
                                  product: pro.products[index],
                                ),
                              ),
                            );
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Image.network(
                                pro.products[index].image,
                                height: 120,
                                width: double.infinity,
                                fit: BoxFit.cover,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  pro.products[index].name,
                                  style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0),
                                child: Text(
                                  pro.products[index].description,
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0,
                                  vertical: 5.0,
                                ),
                                child: Text(
                                  pro.products[index].price,
                                  style: const TextStyle(
                                    color: Colors.green,
                                    fontWeight: FontWeight.bold,
                                  ),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              const Spacer(),
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 8.0),
                                child: Center(
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: [
                                      Expanded(
                                        child: ElevatedButton(
                                          onPressed: () {
                                            // Handle add to cart button press
                                          },
                                          style: ElevatedButton.styleFrom(
                                            backgroundColor: Colors.pinkAccent
                                                .withOpacity(0.7),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(16),
                                            ),
                                          ),
                                          child: Row(
                                                                children: [
                                                                const Text('Add to Cart'),
                                                                IconButton(
                                  icon: Icon(Icons.favorite_border),
                                  onPressed: () {
                                    // Define the action when the button is pressed
                                  },
                                                                ),
                                                              ],
                                                            ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
            ] else ...[
              const Padding(
                padding: EdgeInsets.all(20.0),
                child: Center(child: Text("No products available")),
              ),
            ],
          ],
        ),
      ),
    );
  }
}
