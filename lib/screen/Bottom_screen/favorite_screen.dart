import 'package:cosmetic_app/models/product_model.dart';
import 'package:cosmetic_app/screen/Bottom_screen/menu.dart';
import 'package:cosmetic_app/screen/product_details.dart';
import 'package:cosmetic_app/screen/welcome_screen.dart';
import 'package:flutter/material.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({super.key, this.items});
  final List<Product>? items;
  @override
  State<FavoriteScreen> createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  // Sample data for the cards
  int _selectedIndex = 0;
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (_selectedIndex == 2) {
        // Navigate to Favorites screen
        _navigateToFavoritesScreen();
      }
      if (_selectedIndex == 1) {
        _navigateToMenuScreen();
      }
      if (_selectedIndex == 0) {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => WelcomeScreen(),
            ));
      }
      if (_selectedIndex == 3) {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => WelcomeScreen(),
            ));
      }
    });
  }

  void _navigateToMenuScreen() {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => MenuScreens(),
        ));
  }

  void _navigateToFavoritesScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => FavoriteScreen(
                items: [],
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => WelcomeScreen()));
          },
          child: const Icon(Icons.arrow_back, color: Colors.white),
        ),
        title: const Text(
          'Favorite',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.share, color: Colors.white),
            onPressed: () {},
          ),
        ],
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.deepPurple, Colors.purpleAccent],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.0, 0.7],
              tileMode: TileMode.clamp,
            ),
          ),
        ),
      ),
      body: widget.items != null
          ? ListView.builder(
              itemCount: widget.items?.length,
              itemBuilder: (context, index) {
                return _buildCard(widget.items![index]);
              },
            )
          : const Text("Invalid"),
    );
  }

  Widget _buildCard(Product item) {
    return Card(
      child: ListTile(
        leading: Image.network(
          item.image,
          width: 60,
          height: 60,
          fit: BoxFit.cover,
        ),
        title: Text(item.name),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(item.price),
            const SizedBox(height: 4),
            Text(
              item.description,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ProductDetails(product: item),
              ));
        },
      ),
    );
  }
}
