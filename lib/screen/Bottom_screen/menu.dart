import 'package:cosmetic_app/Repository/product_repository.dart';
import 'package:cosmetic_app/models/product_model.dart';
import 'package:cosmetic_app/screen/Bottom_screen/favorite_screen.dart';
import 'package:cosmetic_app/screen/welcome_screen.dart';
import 'package:flutter/material.dart';
class MenuScreens extends StatefulWidget {
  const MenuScreens({super.key});

  @override
  State<MenuScreens> createState() => _MenuScreensState();
}



int _selectedIndex = 0;
final List<String> categories = ['Cleansers', 'Exfoliators', 'Toners', 'Treatments', 'Masks', 'Moisturizers'];
final List<IconData> categoryIcons = [
  Icons.clean_hands,
  Icons.spa,
  Icons.local_drink,
  Icons.medical_services,
  Icons.face,
  Icons.wash,
];

final List<Map<String, String>> mockProducts = [
  {
    'image': 'https://m.media-amazon.com/images/I/31pxf+yECVL.jpg',
    'name': '3CE COLOR # 221',
    'description': 'A vibrant color lipstick',
    'price': '\$26',
  },
  {
    'image': 'https://m.media-amazon.com/images/I/61UqvcCJGwL._SL1001_.jpg',
    'name': '3CE Beach Muse Eye',
    'description': 'A soft cushion for your skin',
    'price': '\$15',
  },
  {
    'image': 'https://www.kbeautyblossom.com/cdn/shop/products/Youthcoral.png?v=1682616673',
    'name': '3CE ®Lipstick',
    'description': 'A hydrating face mask',
    'price': '\$8',
  },
  {
    'image': 'https://images-na.ssl-images-amazon.com/images/I/41fHc-W%2BGoL.jpg',
    'name': '3CE Soft Lip Lacquer',
    'description': 'A hydrating face mask',
    'price': '\$15',
  },
  {
    'image': 'https://d3i908zd4kzakt.cloudfront.net/data/item/1647564534/thumb-01qjs_500x500.jpg',
    'name': 'Cousion',
    'description': 'A hydrating face mask',
    'price': '\$30',
  },
  {
    'image': 'https://www.elle.vn/wp-content/uploads/2024/05/29/589633/3ce-makeup-fixer-mist-trang-diem-co-dau.jpg',
    'name': '3ce-makeup-fixer',
    'description': 'A hydrating face mask',
    'price': '\$30',
  },
  {
    'image': 'https://m.media-amazon.com/images/I/51Q1VMv-NOL._SL1020_.jpg',
    'name': 'Coussin Blush 3CE ',
    'description': 'A hydrating face mask',
    'price': '\$30',
  },
  {
    'image': 'https://d3i908zd4kzakt.cloudfront.net/data/item/1627962601/thumb-KidsBabyCream400ml_405x405.jpg',
    'name': 'Kids & Baby Cream 400ml ',
    'description': 'A hydrating face mask',
    'price': '\$15.40 ',
  },
  {
    'image': 'https://d3i908zd4kzakt.cloudfront.net/data/item/1606985552/thumb-PYBS12CMB2_405x405.png',
    'name': 'Calming Moisture Barrier Cream 50ml ',
    'description': 'A hydrating face mask',
    'price': '\$9.50 ',
  },
  {
    'image': 'https://assets-cdn.vtenh.com/rails/active_storage/representations/proxy/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBemo5Q0E9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--eda2d5b2f8674164614de662a95c4ea159b032c5/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lJYW5CbkJqb0dSVlE2RTNKbGMybDZaVjloYm1SZmNHRmtXd2RwQWxnQ2FRSllBZz09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--3dee1ed9b6e56500806820ced664e44d84074fae/scaled_image_picker3156592203987874489.jpg?locale=en',
    'name': '3CE Stylenanda Blur Water Tint - ​#Spot Player ',
    'description': 'A hydrating face mask',
    'price': '\$30',
  },
];

class _MenuScreensState extends State<MenuScreens> {
  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (_selectedIndex == 2) {
        // Navigate to Favorites screen
        _navigateToFavoritesScreen();
      }
      if(_selectedIndex == 1){
        _navigateToMenuScreen();
      }
      if(_selectedIndex == 0){
        Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomeScreen(),));
      }
      if(_selectedIndex == 3){
        Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomeScreen(),));
      }
    });
  }
  void _navigateToMenuScreen(){
    Navigator.push(context, MaterialPageRoute(builder: (context) => MenuScreens(),));
  }

  void _navigateToFavoritesScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => FavoriteScreen(items: [],)),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => WelcomeScreen()));
          },
          child: const Icon(Icons.arrow_back, color: Colors.white),
        ),
        title: const Center(
          child: Text(
            'Menu',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.share, color: Colors.white),
            onPressed: () {
              // TODO: Implement share action
            },
          ),
        ],
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.pink, Colors.pinkAccent],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.0, 0.7],
              tileMode: TileMode.clamp,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(
                'What are you looking for?',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: GridView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3, // Number of columns
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 16.0,
                  childAspectRatio: 2 / 2.5,
                ),
                itemCount: categories.length,
                itemBuilder: (context, index) {
                  return Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.pinkAccent),
                      borderRadius: BorderRadius.circular(10),
                      color: index % 2 == 0 ? Colors.pinkAccent.withOpacity(0.1) : Colors.purpleAccent.withOpacity(0.1),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CircleAvatar(
                          radius: 30,
                          backgroundColor: Colors.pinkAccent.withOpacity(0.5),
                          child: Icon(categoryIcons[index], color: Colors.white),
                        ),
                        const SizedBox(height: 5),
                        Text(categories[index]),
                      ],
                    ),
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                '${mockProducts.length} Products',
                style: const TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
            ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: mockProducts.length,
              itemBuilder: (context, index) {
                return Card(
                  child: ListTile(
                    leading: Image.network(mockProducts[index]['image']!),
                    title: Text(mockProducts[index]['name']!),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(mockProducts[index]['description']!),
                        Text('Price: ${mockProducts[index]['price']}'),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
