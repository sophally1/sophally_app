import 'package:cosmetic_app/screen/Bottom_screen/favorite_screen.dart';
import 'package:cosmetic_app/screen/Bottom_screen/menu.dart';
import 'package:cosmetic_app/screen/welcome_screen.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({super.key});

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (_selectedIndex == 0) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => WelcomeScreen(),
          ),
        );
      }
      if (_selectedIndex == 1) {
        _navigateToMenuScreen();
      }
      if (_selectedIndex == 2) {
        // Navigate to Favorites screen
        _navigateToFavoritesScreen();
      }
      if (_selectedIndex == 3) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => const ProfileScreen(),
          ),
        );
      }
    });
  }

  void _navigateToMenuScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const MenuScreens(),
      ),
    );
  }

  void _navigateToFavoritesScreen() {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            FavoriteScreen(
              items: [],
            ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => WelcomeScreen(),
              ),
            );
          },
          child: const Icon(Icons.arrow_back, color: Colors.white),
        ),
        title: const Center(
          child: Text(
            'Profile',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: 'Roboto',
            ),
          ),
        ),
        actions: [
          IconButton(
            icon: const Icon(Icons.share, color: Colors.white),
            onPressed: () {
              // TODO: Implement share action
            },
          ),
        ],
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [Colors.pink, Colors.pinkAccent],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              stops: [0.0, 0.7],
              tileMode: TileMode.clamp,
            ),
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 20),
            const CircleAvatar(
              radius: 50,
              backgroundImage: AssetImage(
                  'assets/images/profile.JPG'), // Replace with actual image path
            ),
            const SizedBox(height: 10),
            const Text(
              'Lee シ', // Replace with user's name
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 5),
            const Text(
              '@l_e_e_b_o_b_e', // Replace with user's handle
              style: TextStyle(
                fontSize: 18,
                color: Colors.grey,
              ),
            ),
            const SizedBox(height: 20),
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text(
                      '9',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Posts',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      '625',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Followers',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Text(
                      '590',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(
                      'Following',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.grey,
                      ),
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 20),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {
                          // Handle add to cart button press
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.pinkAccent.withOpacity(0.5),
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text('edit profile',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            )),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                      width: 20,
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {
                          // Handle add to cart button press
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.pinkAccent.withOpacity(0.5),
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                        child: const Text('share profile',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 13,
                            )),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20), // Add some space before the list
            // List of menu items
            ListView(
              shrinkWrap: true,
              physics: const NeverScrollableScrollPhysics(),
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.person, 'Edit Profile'),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(thickness: 0.3, height: 0.5),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.location_on, 'Shopping Address'),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(thickness: 0.3, height: 0.5),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.favorite, 'Favorite'),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(thickness: 0.3, height: 0.5),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.history, 'Order History'),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(thickness: 0.3, height: 0.5),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.notifications, 'Notification'),
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  child: Divider(thickness: 0.3, height: 0.5),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0), // Adjust vertical padding
                  child: _buildListItem(Icons.credit_card, 'Cards'),
                ),
              ],
            ),

            ElevatedButton(
              onPressed: () {
                // Handle add to cart button press
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.pinkAccent.withOpacity(0.5),
                elevation: 5,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                ),
              ),
              child: const Text('Log out',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 13,
                  )),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildListItem(IconData icon, String title) {
    return ListTile(
      leading: Icon(icon,color: Colors.grey,),
      title: Text(title),
      trailing: const Icon(Icons.arrow_forward_ios,color: Colors.grey,),
      onTap: () {
        // Handle item tap
      },
    );
  }
}
