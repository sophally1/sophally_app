import 'package:carousel_slider/carousel_slider.dart';
import 'package:cosmetic_app/Repository/product_repository.dart';
import 'package:cosmetic_app/models/product_model.dart';
import 'package:cosmetic_app/screen/Bottom_screen/favorite_screen.dart';
import 'package:cosmetic_app/screen/Bottom_screen/menu.dart';
import 'package:cosmetic_app/screen/Bottom_screen/profile_screen.dart';
import 'package:cosmetic_app/screen/product_details.dart';
import 'package:cosmetic_app/screen/views/home_page.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({super.key});

  @override
  State<WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  int _selectedIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  final List<Widget> _widgetOptions = <Widget>[
    const Text('Home Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Discover Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Rewards Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Discuss Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
    const Text('Profile Page', style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold)),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  final List<String> categories = ['Cleansers', 'Exfoliators', 'Toners', 'Treatments', 'Masks', 'Moisturizers'];
  final List<IconData> categoryIcons = [
    Icons.clean_hands,
    Icons.spa,
    Icons.local_drink,
    Icons.medical_services,
    Icons.face,
    Icons.wash,
  ];

  List<Widget> screens = [
    const HomePage(),
    const MenuScreens(),
    const FavoriteScreen(),
    const ProfileScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screens[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        items: [
          BottomNavigationBarItem(
            icon: _selectedIndex == 0
                ? const Icon(Icons.home, color: Colors.purpleAccent)
                : const Icon(Icons.home, color: Colors.grey),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 1
                ? const Icon(Icons.menu, color: Colors.purpleAccent)
                : const Icon(Icons.menu, color: Colors.grey),
            label: 'Menu',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 2
                ? const Icon(Icons.favorite, color: Colors.purpleAccent)
                : const Icon(Icons.favorite, color: Colors.grey),
            label: 'favourite',
          ),
          BottomNavigationBarItem(
            icon: _selectedIndex == 3
                ? const Icon(Icons.person, color: Colors.purpleAccent)
                : const Icon(Icons.person, color: Colors.grey),
            label: 'Profile',
          ),
        ],
        selectedItemColor: Colors.purpleAccent,
        unselectedItemColor: Colors.grey,
        showUnselectedLabels: true,
      ),
    );
  }
}
