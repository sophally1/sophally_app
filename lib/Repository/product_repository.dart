import 'dart:convert';
import 'package:cosmetic_app/models/product_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
class ProductRepository {
  Future<List<Product>?> fetchProducts() async {
    await Future.delayed(const Duration(seconds: 1));
    final String response = await rootBundle.loadString('assets/products.json');
    final Map<String, dynamic> jsonData = jsonDecode(response);

    if(jsonData.isNotEmpty){
      List<Product> product = [];
      debugPrint("==========&& $jsonData");
      for(var p in jsonData['mockProducts']){
        product.add(Product.fromJson(p));
      }
      return product;
    }
    return null;
  }
}